#-------------------------------------------------
#
# Project created by QtCreator 2015-08-11T22:53:20
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = AlpariAnalyze
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    matrix.cpp \
    alglib/linalg.cpp \
    alglib/ap.cpp \
    alglib/alglibmisc.cpp \
    alglib/alglibinternal.cpp \
    alglib/statistics.cpp \
    alglib/specialfunctions.cpp \
    alglib/solvers.cpp \
    alglib/optimization.cpp \
    alglib/interpolation.cpp \
    alglib/integration.cpp \
    alglib/fasttransforms.cpp \
    alglib/diffequations.cpp \
    alglib/dataanalysis.cpp

HEADERS += alglib/linalg.h \
    alglib/ap.h \
    alglib/alglibmisc.h \
    alglib/alglibinternal.h \
    alglib/stdafx.h \
    alglib/statistics.h \
    alglib/specialfunctions.h \
    alglib/solvers.h \
    alglib/optimization.h \
    alglib/interpolation.h \
    alglib/integration.h \
    alglib/fasttransforms.h \
    alglib/diffequations.h \
    alglib/dataanalysis.h \
    myexception.h
