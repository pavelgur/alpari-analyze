#include <QCoreApplication>
#include <QFile>
#include <QDateTime>
#include <QStringList>
#include <QTextStream>
#include <QVector>
#include <QtMath>
#include <QSet>
#include <QDebug>
#include <algorithm>
#include <iostream>

#include "matrix.h"

enum class EPeriodType {
    DAY,
    HOUR,
};

struct TParsedInfo {
    TParsedInfo() = default;
    TParsedInfo(const QDateTime& dateTime, const qreal profit) noexcept
        : DateTime(dateTime)
        , Profit(profit)
    {}

    TParsedInfo(const QString& line, const EPeriodType type) noexcept {
        const auto splitted = line.split(';');
        switch (type) {
        case EPeriodType::DAY:
            DateTime = QDateTime::fromString(splitted[0], "yyyy-MM-dd");
            break;
        case EPeriodType::HOUR:
            DateTime = QDateTime::fromString(splitted[0], "yyyy-MM-dd hh:mm");
            break;
        }
        Profit = (splitted[4].toDouble() - splitted[1].toDouble()) / 100;
    }

    QDateTime DateTime;
    qreal Profit;
};

template<typename TCont>
static qreal CalcSharpeCoef(const TCont& profits, const double riskFree) noexcept {
    if (profits.size() <= 1)
        return 0;

    const auto& N = profits.size();

    qreal mean = 0;
    for (const auto& v : profits) {
        mean += v.Profit;
    }
    mean /= N;

    qreal stdDev = 0;
    for (const auto& v : profits) {
        stdDev += pow(v.Profit - mean, 2);
    }

    stdDev = qSqrt(stdDev / (N - 1));

    return (mean - riskFree) / stdDev;
}

template<typename TCont>
static qreal CalcLossProbability(const TCont& profits) noexcept {
    if (profits.empty())
        return 0;

    auto lossCount = 0u;

    for (const auto& v : profits) {
        if (v.Profit < 0)
            ++lossCount;
    }

    return static_cast<qreal>(lossCount) / profits.size();
}

template<typename TCont>
static qreal CalcAvgLoss(const TCont& profits) noexcept {
    if (profits.empty())
        return 0;

    auto lossCount = 0u;

    qreal sum = 0;
    for (const auto& v : profits) {
        if (v.Profit < 0) {
            ++lossCount;
            sum += v.Profit;
        }
    }

    if (lossCount == 0)
        return 0;

    return sum / static_cast<qreal>(lossCount);
}

template<typename TCont>
static qreal CalcAvgProfit(const TCont& profits) noexcept {
    if (profits.empty())
        return 0;

    auto profitCount = 0u;

    qreal sum = 0;
    for (const auto& v : profits) {
        if (v.Profit > 0) {
            ++profitCount;
            sum += v.Profit;
        }
    }

    if (profitCount == 0)
        return 0;

    return sum / static_cast<qreal>(profitCount);
}

static constexpr qreal CalcRiskFree(const qreal mult) noexcept {
    return 0.1 / (365. * 5 / 7 * mult);
}

static QVector<qreal> CalcMarkovicParts(const TMatrix& cov, const TVector& profits, const qreal mult, const qreal money = 0, const qreal profit = 0) {
    const auto covInv = cov.Inverted();

    TMatrix re(2, profits.GetLength(), 1);
    for (auto i = 0u; i < profits.GetLength(); ++i) {
        re[0][i] = profits[i];
    }

    const TMatrix A = re * covInv * re.Transposed();

    const auto wantedProfit = (profit > 0 ? profit : A[0][0] / A[0][1]);
    printf("Day profit: %lf%%, %lf$\n", 100 * wantedProfit * mult, wantedProfit * money * mult);
    printf("Month profit: %lf%%, %lf$\n", 100 * (pow(1 + wantedProfit * mult, 20) - 1), (pow(1 + wantedProfit * mult, 20) - 1) * money);
    printf("Year profit: %lf%%, %lf$\n", 100 * (pow(1 + wantedProfit * mult, 52 * 5) - 1), (pow(1 + wantedProfit * mult, 12 * 20) - 1) * money);

    TVector dummy(2, 1);
    dummy[0] = wantedProfit;
    const TMatrix parts = covInv * re.Transposed() * A.Inverted() * dummy;

    QVector<qreal> res((parts.GetRows()));
    {
        qreal sum = 0;
        for (auto i = 0; i < res.size(); ++i) {
            sum += (res[i] = qMax(parts[i][0], 0.0));
        }
        for (auto i = 0; i < res.size(); ++i) {
            res[i] /= sum;
        }
    }

    return res;
}

struct TStrategyInfo {
    TStrategyInfo() = default;

    TStrategyInfo(const QString& name) noexcept
        : Name(name)
    {}

    bool operator<(const TStrategyInfo& other) const noexcept {
        return Part >= other.Part;
    }

    QString Name;
    QList<TParsedInfo> History;
    qreal Comission = 0;
    qreal Part = 0;
    qreal Profit = 0;
    qreal StdDev = 0;
    qreal LossProb = 0;
    qreal Sharpe = 0;
    qreal AvgLoss = 0;
    qreal AvgProfit = 0;
};

static void Calc(QVector<TStrategyInfo>&& strPool, const double sumOfMoney, const double wantedProfit, const EPeriodType type) {
    {
        TMatrix cov(strPool.size(), strPool.size(), 0);
        TVector profits(strPool.size(), 0);

        // calculating profits
        for (auto i = 0; i < strPool.size(); ++i) {
            for (auto k = 0; k < strPool[i].History.size(); ++k) {
                profits[i] += strPool[i].History[k].Profit;
            }
            profits[i] /= strPool[i].History.size();
        }

        // calculating cov matrix
        for (auto i = 0; i < strPool.size(); ++i) {
            for (auto j = i; j < strPool.size(); ++j) {
                for (auto k = 0; k < strPool[i].History.size(); ++k) {
                    cov[i][j] += (strPool[i].History[k].Profit - profits[i]) * (strPool[j].History[k].Profit - profits[j]);
                }
                cov[i][j] /= strPool[i].History.size();
                cov[j][i] = cov[i][j];
            }
        }

        for (auto i = 0; i < strPool.size(); ++i) {
            profits[i] *= (1 - strPool[i].Comission);
        }

        const auto typeMult = (type == EPeriodType::HOUR ? 24 : 1);

        const auto parts = CalcMarkovicParts(cov, profits, typeMult, sumOfMoney, wantedProfit);
        for (auto i = 0; i < strPool.size(); ++i) {
            const auto hitSize = strPool[i].History.size();
            strPool[i].Part = parts[i];
            strPool[i].Profit = profits[i];
            strPool[i].StdDev = qSqrt(cov[i][i] * hitSize / (hitSize - 1));
            strPool[i].Sharpe = CalcSharpeCoef(strPool[i].History, CalcRiskFree(typeMult));
            strPool[i].LossProb = CalcLossProbability(strPool[i].History);
            strPool[i].AvgLoss = CalcAvgLoss(strPool[i].History);
            strPool[i].AvgProfit = CalcAvgProfit(strPool[i].History);
        }

        QVector<TParsedInfo> balance;
        for (auto i = 0; i < strPool.front().History.size(); ++i) {
            qreal delta = 0;
            for (auto j = 0; j < strPool.size(); ++j) {
                delta += strPool[j].History[i].Profit * strPool[j].Part;
            }
            balance.push_back(TParsedInfo(QDateTime(), delta));
        }

        printf("Portfolio Stats\n\tSharpe: %lf\n\tLoss Probability: %lf%%\n\tAvgLoss: %lf%%\n\tAvgProfit: %lf%%\n",
               CalcSharpeCoef(balance, CalcRiskFree(typeMult)),
               100 * CalcLossProbability(balance),
               100 * CalcAvgLoss(balance),
               100 * CalcAvgProfit(balance));
    }

    std::sort(strPool.begin(), strPool.end());

    for (const auto& str : strPool) {
        printf(
                    "%s:\n\tpart=%lf,"
                    "investment=%lf,"
                    "profit=%lf%%,"
                    "stdDev = %lf%%,"
                    "Sharpe = %lf,"
                    "LossProb = %lf%%,"
                    "AvgProfit = %lf%%,"
                    "AvgLoss = %lf%%\n",
                    str.Name.toLocal8Bit().data(),
                    str.Part,
                    str.Part * sumOfMoney,
                    100 * str.Profit,
                    100 * str.StdDev,
                    str.Sharpe,
                    100 * str.LossProb,
                    100 * str.AvgProfit,
                    100 * str.AvgLoss);
    }
}

static EPeriodType Parse(int& argc, char* argv[], QVector<TStrategyInfo>& strPool, double& sumOfMoney, double& wantedProfit) {
    QCoreApplication app(argc, argv);
    strPool.clear();

    const auto type = [&] () {
        if (app.arguments()[1].toLower() == "d")
            return EPeriodType::DAY;
        if (app.arguments()[1].toLower() == "h")
            return EPeriodType::HOUR;
        throw myexception() << "first arg should be h (hour) or d (day)";
    } ();

    int firstIndex = 2;
    {
        auto ok = false;
        sumOfMoney = QString(app.arguments()[firstIndex]).toDouble(&ok); ///  default 0
        if (ok)
            ++firstIndex;
    }

    {
        auto ok = false;
        wantedProfit = QString(app.arguments()[firstIndex]).toDouble(&ok) / 100; // default: 0
        if (ok)
            ++firstIndex;
    }

    for (auto i = firstIndex; i < app.arguments().size(); ++i) {
        strPool.push_back(TStrategyInfo(app.arguments()[i].split('/').back()));
        auto& strInfo = strPool.back();

        QFile file(app.arguments()[i]);
        file.open(QIODevice::ReadOnly | QIODevice::Text);

        QTextStream stream(&file);
        {
            auto ok = false;
            strInfo.Comission = stream.readLine().toDouble(&ok);
            if (!ok)
                throw myexception() << "First line of each file should be comission level";
        }

        for (QString line; !stream.atEnd() && (line = stream.readLine()).size() > 0;) {
            const auto info = TParsedInfo(line, type);
            if (info.DateTime.date().dayOfWeek() < 6)
                strInfo.History.push_back(info);
        }
        if (strInfo.History.empty())
            throw myexception() << "Strategy " << strPool.back().Name << " has empty history!";
    }

    switch (type) {
    case EPeriodType::DAY: { // alignment of periods
        QDate minDate = strPool.front().History.front().DateTime.date();
        QDate maxDate = strPool.front().History.back().DateTime.date();
        for (const auto& str : strPool) {
            minDate = qMax(minDate, str.History.front().DateTime.date());
            maxDate = qMax(maxDate, str.History.back().DateTime.date());
        }

        for (auto& str : strPool) {
            while (str.History.size() > 0 && str.History.front().DateTime.date() < minDate) {
                str.History.pop_front();
            }
            if (str.History.empty())
                throw myexception() << "Bad strategy " << str.Name << " history: incompatible with others.";

            if (str.History.back().DateTime.date() != maxDate)
                str.History.push_back(TParsedInfo(QDateTime(maxDate), 0));

            for (auto it = str.History.begin(); it + 1 != str.History.end(); ++it) {
                while (it->DateTime.date().daysTo((it + 1)->DateTime.date()) > 1) {
                    it = str.History.insert(it + 1, TParsedInfo(QDateTime(it->DateTime.date().addDays(1)), 0));
                }
            }
        }
        break;
    }
    case EPeriodType::HOUR: {
        const auto size = strPool.front().History.size();
        for (const auto& str : strPool) {
            if (str.History.size() != size)
                throw myexception() << "Misaligned data!";
        }
        break;
    }
    }

    return type;
}

int main(int argc, char *argv[]) {
    QVector<TStrategyInfo> strPool;
    double sumOfMoney;
    double wantedProfit;

    const auto type = Parse(argc, argv, strPool, sumOfMoney, wantedProfit);
    Calc(std::move(strPool), sumOfMoney, wantedProfit, type);

    return EXIT_SUCCESS;
}
