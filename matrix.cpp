#include "matrix.h"
#include "cmath"

TMatrix operator*(TMatrix a, const TMatrix& b) noexcept {
    a *= b;
    return a;
}

TMatrix operator*(TMatrix a, const qreal& b) noexcept {
    a *= b;
    return a;
}

TMatrix operator/(TMatrix a, const qreal& b) noexcept {
    a /= b;
    return a;
}

TMatrix operator+(TMatrix a, const TMatrix& b) noexcept {
    a += b;
    return a;
}

TMatrix operator-(TMatrix a, const TMatrix& b) noexcept {
    a -= b;
    return a;
}

TMatrix operator+(TMatrix a, const qreal& b) noexcept {
    a += b;
    return a;
}

TMatrix operator-(TMatrix a, const qreal& b) noexcept {
    a -= b;
    return a;
}

TVector operator*(TVector a, const qreal& b) noexcept {
    a *= b;
    return a;
}

TVector operator/(TVector a, const qreal& b) noexcept {
    a /= b;
    return a;
}

TVector operator+(TVector a, const TVector& b) noexcept {
    a += b;
    return a;
}

TVector operator-(TVector a, const TVector& b) noexcept {
    a -= b;
    return a;
}

TVector operator+(TVector a, const qreal& b) noexcept {
    a += b;
    return a;
}

TVector operator-(TVector a, const qreal& b) noexcept {
    a -= b;
    return a;
}

TMatrix TMatrix::Inverted() const {
    if (Rows != Columns || Rows == 0)
        throw myexception() << "Failed to inverse matrix: not squared";

    alglib::matinvreport report;
    long int info = 0;

    auto m = Data;
    alglib::rmatrixinverse(m, info, report);
    if (info != 1)
        throw myexception() << "Failed to inverse matrix: bad-conditioned matrix (code:" << QString::number(info).toLocal8Bit().data() << ")";
    return TMatrix(m);
}

qreal TMatrix::Determinant() const {
    if (Rows != Columns)
        throw myexception() << "Failed to get determinant of non-squared matrix";

    qreal det = 1;
    auto temp = Data;
    for (int i = 0; i < Data.rows(); ++i) {
        int k = i;
        for (int j = i + 1; j < Data.rows(); ++j)
            if (qAbs(temp[j][i]) > qAbs(temp[k][i]))
                k = j;
        if (qAbs(temp[k][i]) < 1e-12) {
            det = 0;
            break;
        }
        std::swap_ranges(temp[i], temp[i] + Columns, temp[k]);
        if (i != k)
            det = -det;
        det *= temp[i][i];
        for (int j = i+1; j < Data.rows(); ++j)
            temp[i][j] /= temp[i][i];
        for (int j = 0; j < Data.rows(); ++j)
            if (j != i && qAbs(temp[j][i]) > 1e-12)
                for (int k = i + 1; k < Data.rows(); ++k)
                    temp[j][k] -= temp[i][k] * temp[j][i];
    }
    return det;
}

TMatrix TMatrix::Transposed() const noexcept {
    TMatrix tr(Columns, Rows);
    for (size_t i = 0; i < Columns; ++i) {
        for (size_t j = 0; j < Rows; ++j) {
            tr[i][j] = Data[j][i];
        }
    }
    return tr;
}

TMatrix::TMatrix(const size_t rows, const size_t columns, const qreal value)
    : Rows(rows)
    , Columns(columns)
{
    if (Rows == 0 || Columns == 0)
        throw myexception() << "Error while matrix defining";
    Data.setlength(Rows, Columns);
    for(auto i = 0u; i < Rows; ++i) {
        for(auto j = 0u; j < Columns; ++j) {
            Data[i][j] = value;
        }
    }
}

qreal TVector::operator()(const TVector& v) const { // dot product
    if (GetLength() != v.GetLength())
        throw myexception() << "Dot product is defined for vectors only of same dimension.";
    qreal dotProduct = 0;
    for (size_t i = 0; i < Rows; ++i) {
        dotProduct += Data[i][0] * v[i];
    }
    return dotProduct;
}

TMatrix& TMatrix::operator*=(const qreal& mult) noexcept {
    for (size_t i = 0; i < Rows; ++i) {
        for (size_t j = 0; j < Columns; ++j) {
            Data[i][j] *= mult;
        }
    }
    return *this;
}

TMatrix& TMatrix::operator/=(const qreal& mult) noexcept {
    for (size_t i = 0; i < Rows; ++i) {
        for (size_t j = 0; j < Columns; ++j) {
            Data[i][j] /= mult;
        }
    }
    return *this;
}

TMatrix& TMatrix::operator+=(const qreal& mult) noexcept {
    for (size_t i = 0; i < Rows; ++i) {
        for (size_t j = 0; j < Columns; ++j) {
            Data[i][j] += mult;
        }
    }
    return *this;
}

TMatrix& TMatrix::operator-=(const qreal& mult) noexcept {
    for (size_t i = 0; i < Rows; ++i) {
        for (size_t j = 0; j < Columns; ++j) {
            Data[i][j] -= mult;
        }
    }
    return *this;
}

TMatrix& TMatrix::operator*=(const TMatrix& m) {
    if (Columns != m.Rows)
        throw myexception() << "Cannot multiply matrices: wrong sizes";
    TMatrix res(Rows, m.Columns);
    for(size_t row = 0; row < res.Rows; ++row) {
        for (size_t col = 0; col < res.Columns; ++col) {
            for (size_t k = 0; k < Columns; ++k) {
                res[row][col] += Data[row][k] * m.Data[k][col];
            }
        }
    }
    *this = res;
    return *this;
}

TMatrix& TMatrix::operator+=(const TMatrix& m) {
    if (Rows != m.Rows || Columns != m.Columns) {
        throw myexception() << "Cannot summ matrices: wrong sizes";
    }
    for (size_t row = 0; row < Rows; ++row) {
        for(size_t col = 0; col < Columns; ++col) {
            Data[row][col] += m.Data[row][col];
        }
    }
    return *this;
}

TMatrix& TMatrix::operator-=(const TMatrix& m) {
    if (Rows != m.Rows || Columns != m.Columns) {
        throw myexception() << "Cannot sub matrices: wrong sizes";
    }
    for (size_t row = 0; row < Rows; ++row) {
        for(size_t col = 0; col < Columns; ++col) {
            Data[row][col] -= m.Data[row][col];
        }
    }
    return *this;
}

TVector& TVector::operator*=(const qreal& mult) noexcept {
    for (size_t i = 0; i < Rows; ++i) {
        Data[i][0] *= mult;
    }
    return *this;
}

TVector& TVector::operator/=(const qreal& mult) noexcept {
    for (size_t i = 0; i < Rows; ++i) {
        Data[i][0] /= mult;
    }
    return *this;
}

TVector& TVector::operator+=(const qreal& mult) noexcept {
    for (size_t i = 0; i < Rows; ++i) {
        for (size_t j = 0; j < Columns; ++j) {
            Data[i][j] += mult;
        }
    }
    return *this;
}

TVector& TVector::operator-=(const qreal& mult) noexcept {
    for (size_t i = 0; i < Rows; ++i) {
        for (size_t j = 0; j < Columns; ++j) {
            Data[i][j] -= mult;
        }
    }
    return *this;
}

TVector& TVector::operator<<(const qreal& v) noexcept {
    alglib::real_2d_array newData;
    newData.setlength(Data.rows() + 1, Data.cols());

    for (auto i = 0u; i < Rows; ++i) {
        newData[i][0] = Data[i][0];
    }
    newData[Rows][0] = v;
    Data = newData;
    ++Rows;

    return *this;
}

TVector& TVector::operator<<(const TVector& v) noexcept {
    alglib::real_2d_array newData;
    newData.setlength(Data.rows() + v.Rows, Data.cols());

    for (auto i = 0u; i < Rows; ++i) {
        newData[i][0] = Data[i][0];
    }
    for (auto i = 0u; i < v.Rows; ++i) {
        newData[Rows + i][0] = v.Data[i][0];
    }

    Data = newData;
    Rows += v.Rows;

    return *this;
}

TVector& TVector::operator+=(const TVector& m) {
    if (Rows != m.Rows || Columns != m.Columns) {
        throw myexception() << "Cannot sum vectors: wrong sizes";
    }
    for (size_t row = 0; row < Rows; ++row) {
        Data[row][0] += m.Data[row][0];
    }
    return *this;
}

TVector& TVector::operator-=(const TVector& m) {
    if (Rows != m.Rows || Columns != m.Columns) {
        throw myexception() << "Cannot sub vectors: wrong sizes";
    }
    for (size_t row = 0; row < Rows; ++row) {
        Data[row][0] -= m.Data[row][0];
    }
    return *this;
}
