#pragma once

#include <exception>
#include <QString>

class myexception : public std::exception {
public:
    myexception(const QString& what = "") noexcept
        : WAT(what)
    {}

    const char* what() const noexcept override {
        return WAT.toLocal8Bit().data();
    }

    myexception& operator<< (const char* s) throw(std::bad_alloc) {
        WAT.append(s);
        return *this;
    }

    myexception& operator<< (const QString& s) throw(std::bad_alloc) {
        WAT.append(s);
        return *this;
    }

private:
    QString WAT;
};
